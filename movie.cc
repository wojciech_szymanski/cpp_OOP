#include <iostream>
#include "movie.h"
#include "playerException.h"

namespace {
	bool isContentGood(const std::string *content)
	{
		for (auto &c : *content) {
			if (!std::isalnum(c) && !std::isblank(c) && c != ',' && c != '.' && c != '!'
				&& c != '?' && c != '\'' && c != ':' && c != ';' && c != '-')
				return false;
		}
		return true;
	}
}

Movie::Movie (std::string title, std::string year, std::string content):
		year(std::move(year)), title(std::move(title)), content(std::move(content)) {}

void Movie::play()
{
	std::cout << "Movie [" << title << ", " << year << "]: " << content << std::endl;
}

bool Movie::isValid()
{
    return true;
}

std::shared_ptr<Playable> Movie::getShared() {return std::make_shared<Movie>(*this);}

Movie::Movie(const File &file)
{
	const std::string *title = file.getField("title"), *year = file.getField("year"),
			*content = file.getField(fileContentString);
	if (year == nullptr || title == nullptr || content == nullptr)
		throw MissingDataException();
	else if (!isContentGood(content))
		throw BadContentException();
	this->title = *title, this->year = *year, this->content = *content;
}