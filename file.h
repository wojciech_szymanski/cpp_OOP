#ifndef JNP1_6_FILE_H
#define JNP1_6_FILE_H

#include <string>
#include <map>

const std::string fileContentString = "|content:", fileTypeString = "|type:";

class File
{
public:
    explicit File(const std::string &description);
	const std::string* getField(const std::string &field) const;
private:
	std::map<std::string, std::string> fileData;
};

#endif //JNP1_6_FILE_H
