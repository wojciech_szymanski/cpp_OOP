#ifndef JNP1_6_MODE_H
#define JNP1_6_MODE_H

#include <vector>
#include <memory>
#include "playable.h"

class Mode
{
public:
	using playlist_t = typename std::vector<std::shared_ptr<Playable>>;
	virtual void play(const playlist_t&) = 0;
};


#endif //JNP1_6_MODE_H
