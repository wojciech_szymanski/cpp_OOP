#include "media.h"

bool Media::isContentGood(const std::string *content)
{
	for (auto &c : *content) {
		if (!std::isalnum(c) && !std::isblank(c) && c != ',' && c != '.' && c != '!'
			&& c != '?' && c != '\'' && c != ':' && c != ';' && c != '-')
			return false;
	}
	return true;
}