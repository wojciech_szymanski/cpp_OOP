#ifndef JNP1_6_SONG_H
#define JNP1_6_SONG_H

#include <string>
//#include "media.h"
#include "playable.h"
#include "file.h"

//class Song: public Media
class Song: public Playable
{
public:
    void play() override;
    bool isValid() override;
    Song (std::string, std::string, std::string);
    std::shared_ptr<Playable> getShared() override;
    explicit Song(const File&);
private:
	std::string singer, title, content;
};


#endif //JNP1_6_SONG_H
