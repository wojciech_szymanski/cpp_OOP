#ifndef JNP1_6_PLAYABLE_H
#define JNP1_6_PLAYABLE_H

#include <iostream>
#include <memory>

class Playable
{
public:
    /**
     * @brief plays tracks on the list according to mode's order
     *
     */
    virtual void play() { std::cout << "Base Playable play()\n"; }; //odtwarza utwory na liście zgodnie z zadaną
    // kolejnością:
//    virtual void play() = 0; //odtwarza utwory na liście zgodnie z zadaną kolejnością:
    // (a) sekwencyjnie - zgodnie z kolejnością na liście odtwarzania,
    // (b) losowo - zgodnie z kolejnością określoną przez std::shuffle wywołane z std::default_random_engine,
    // (c)  nieparzyste/parzyste - najpierw wszystkie nieparzyste, a następnie wszystkie parzyste elementy listy
    // odtwarzania.
    virtual bool isValid()
    {
        std::cout << "Base Playable isValid()\n";
        return true;
    }
    virtual std::shared_ptr<Playable> getShared() {std::cout << "ERROR\n"; return std::make_shared<Playable>(*this);}
};


#endif //JNP1_6_PLAYABLE_H
