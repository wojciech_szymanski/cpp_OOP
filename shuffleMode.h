#ifndef JNP1_6_SHUFFLEMODE_H
#define JNP1_6_SHUFFLEMODE_H

#include "mode.h"
#include <random>
#include <algorithm>

class ShuffleMode: public Mode
{
private:
	std::default_random_engine engine;
public:

	ShuffleMode() = delete;

	explicit ShuffleMode(int seed)
	{
		engine = std::default_random_engine(seed);
	}

	void play(const playlist_t &playlist) override
	{
		std::vector<int> indexes;
		for (int i = 0; i < playlist.size(); i++)
			indexes.push_back(i);
		std::shuffle(indexes.begin(), indexes.end(), engine);
		for (int &id : indexes)
			playlist[id]->play();
	}
};


#endif //JNP1_6_SHUFFLEMODE_H
