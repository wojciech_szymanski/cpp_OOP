#ifndef JNP1_6_SEQUENCEMODE_H
#define JNP1_6_SEQUENCEMODE_H

#include <iostream>
#include "mode.h"

class SequenceMode: public Mode
{
public:
	void play(const playlist_t &playlist) override
	{
		for (auto &playable : playlist)
			playable->play();
	}
};

#endif //JNP1_6_SEQUENCEMODE_H
