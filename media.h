#ifndef JNP1_6_MEDIA_H
#define JNP1_6_MEDIA_H

#include "playable.h"
#include "file.h"
#include "playerException.h"

class Media: public Playable
{
	bool isContentGood(const std::string *content);
    virtual Playable* makeFromFile(const File&) {throw PlayerException();}
};


#endif //JNP1_6_MEDIA_H
