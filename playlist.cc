#include <iostream>
#include "playlist.h"
#include "sequenceMode.h"
#include "song.h"
#include "movie.h"
#include "playerException.h"

Playlist::Playlist(std::string name): name(std::move(name)), playMode(new SequenceMode()) {}

void Playlist::add(Playable *element)
{
	playlist.push_back(element->getShared());
}

void Playlist::add(Playable *element, int i)
{
	playlist.insert(playlist.begin() + i, element->getShared());
}

void Playlist::remove()
{
	if (playlist.empty())
		throw OutOfRangeException();
	playlist.pop_back();
}

void Playlist::remove(int position)
{
	if (playlist.size() <= position)
		throw OutOfRangeException();
	playlist.erase(playlist.begin() + position);
}

void Playlist::setMode(Mode *mode)
{
	playMode = mode;
}

void Playlist::play()
{
	std::cout << "Playing [" << name << "]" << std::endl;
	playMode->play(playlist);
}

bool Playlist::isValid()
{
    // todo use to check before adding new element
    for (auto &el: playlist)
    {
        auto pl = dynamic_cast<Playlist*>(el.get());
        if (pl && pl == this)
        {
            return false;
        }
        else if (pl && !pl->isValid())
        {
            return false;
        }
    }
    return true;
}

std::shared_ptr<Playable> Playlist::getShared() {return std::make_shared<Playlist>(*this);}
