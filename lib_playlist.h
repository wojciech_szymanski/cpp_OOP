#ifndef JNP1_6_LIB_PLAYLIST_H
#define JNP1_6_LIB_PLAYLIST_H

#include <string>

#include "file.h"
#include "playlist.h"
#include "song.h"
#include "sequenceMode.h"
#include "shuffleMode.h"
#include "oddEvenMode.h"

class Player
{

public:
    Playable* openFile(const File&);
    Playlist* createPlaylist(std::string playlistName);

};

SequenceMode* createSequenceMode();
ShuffleMode* createShuffleMode(int seed);
OddEvenMode* createOddEvenMode();

#endif //JNP1_6_LIB_PLAYLIST_H
