#include <iostream>
#include "song.h"
#include "playerException.h"

namespace {
	bool isContentGood(const std::string *content)
	{
		for (auto &c : *content) {
			if (!std::isalnum(c) && !std::isblank(c) && c != ',' && c != '.' && c != '!'
				&& c != '?' && c != '\'' && c != ':' && c != ';' && c != '-')
				return false;
		}
		return true;
	}
}

Song::Song (std::string singer, std::string title, std::string content) :
	singer(std::move(singer)), title(std::move(title)), content(std::move(content)) {}

Song::Song (const File &file)
{
	const std::string *artist = file.getField("artist"), *title = file.getField("title"),
			*content = file.getField(fileContentString);
	if (artist == nullptr || title == nullptr || content == nullptr)
		throw MissingDataException();
	else if (!isContentGood(content))
		throw BadContentException();
	this->singer = *artist, this->title = *title, this->content = *content;
}

void Song::play()
{
	std::cout << "Song [" << singer << ", " << title << "]: " << content << std::endl;
}

bool Song::isValid()
{
    return true;
}

std::shared_ptr<Playable> Song::getShared() {return std::make_shared<Song>(*this);}
