#include <sstream>
#include <vector>
#include "file.h"

File::File(const std::string &description)
{
	if (description.empty())
		return;

	std::istringstream iss(description);
	std::string fieldName, fieldContent;

	std::getline(iss, fieldName, '|');
	fileData[fileTypeString] = fieldName;

	while (std::getline(iss, fieldName, ':'))
	{
		if (iss.rdbuf()->in_avail() == 0)
		{
			fileData[fileContentString] = fieldName;
			return;
		}
		std::getline(iss, fieldContent, '|');
		fileData[fieldName] = fieldContent;
	}

	fileData[fileContentString] = fieldName;
}

const std::string* File::getField(const std::string &field) const
{
	auto ptr = fileData.find(field);
	if (ptr == fileData.end())
		return nullptr;
	return &(ptr->second);
}