#include "lib_playlist.h"
#include "movie.h"
#include "playerException.h"

namespace {
	bool isContentGood(const std::string *content)
	{
		for (auto &c : *content) {
			if (!std::isalnum(c) && !std::isblank(c) && c != ',' && c != '.' && c != '!'
				&& c != '?' && c != '\'' && c != ':' && c != ';' && c != '-')
				return false;
		}
		return true;
	}
}

Playable* Player::openFile(const File &file)
{
	const std::string *result = file.getField(fileTypeString);
	if (result == nullptr)
	{
		throw BadFileTypeException();
	}

	if (*result == "audio")
	{
		return new Song(file);
	}
	else if (*result == "video")
	{
		return new Movie(file);
	}
	else
	{
		throw BadFileTypeException();
	}
}

Playlist* Player::createPlaylist(std::string playlistName)
{
	return new Playlist(std::move(playlistName));
}

SequenceMode* createSequenceMode() {return new SequenceMode();}
ShuffleMode* createShuffleMode(int seed) {return new ShuffleMode(seed);}
OddEvenMode* createOddEvenMode() {return new OddEvenMode();}