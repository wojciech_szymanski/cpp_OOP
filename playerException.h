#ifndef JNP1_6_PLAYEREXCEPTION_H
#define JNP1_6_PLAYEREXCEPTION_H

#include <exception>

class PlayerException: public std::exception
{

};

class BadFileTypeException: public PlayerException
{

};

class MissingDataException: public PlayerException
{

};

class BadContentException: public PlayerException
{

};

class OutOfRangeException: public PlayerException
{

};


#endif //JNP1_6_PLAYEREXCEPTION_H
