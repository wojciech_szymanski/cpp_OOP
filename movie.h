#ifndef JNP1_6_MOVIE_H
#define JNP1_6_MOVIE_H


#include <string>
//#include "media.h"
#include "playable.h"
#include "file.h"
#include <memory>

class Movie: public Playable
{
public:
	void play() override;
    bool isValid() override;
    Movie (std::string, std::string, std::string);
	std::shared_ptr<Playable> getShared() override;
	explicit Movie(const File&);
private:
	std::string title, year, content;
};


#endif //JNP1_6_MOVIE_H
