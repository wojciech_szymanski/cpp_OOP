#ifndef JNP1_6_PLAYLIST_H
#define JNP1_6_PLAYLIST_H

#include <vector>
#include "mode.h"
#include "playable.h"

class Playlist: public Playable
{

private:
	using playlist_t = typename std::vector<std::shared_ptr<Playable>>;
    Mode *playMode;
    playlist_t playlist;
    std::string name;

public:
	explicit Playlist(std::string);
    void add(Playable *element); //dodaje element na koniec listy odtwarzania;

    void add(Playable *element, int i); //dodaje element na określonej pozycji w liście odtwarzania (pozycje
    // są
    // numerowane od
    // 0);

    void remove(); //usuwa ostatni element z listy odtwarzania;

    void remove(int position); //usuwa element z określonej pozycji listy odtwarzania.

    void setMode(Mode *mode); //ustawia sposób (kolejność) odtwarzania utworów; sposób odtwarzania może zostać
    // utworzony za
    // pomocą funkcji createSequenceMode(), createShuffleMode(seed), createOddEvenMode().

    void play() override; //odtwarza utwory na liście zgodnie z zadaną kolejnością:
    // (a) sekwencyjnie - zgodnie z kolejnością na liście odtwarzania,
    // (b) losowo - zgodnie z kolejnością określoną przez std::shuffle wywołane z std::default_random_engine,
    // (c)  nieparzyste/parzyste - najpierw wszystkie nieparzyste, a następnie wszystkie parzyste elementy listy
    // odtwarzania.
    bool isValid() override;
	std::shared_ptr<Playable> getShared() override;
};


#endif //JNP1_6_PLAYLIST_H
