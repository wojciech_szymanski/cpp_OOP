#ifndef JNP1_6_ODDEVENMODE_H
#define JNP1_6_ODDEVENMODE_H

class OddEvenMode: public Mode
{
public:
	void play(const playlist_t &playlist) override
	{
		for (int start = 1; start >= 0; start--)
			for (auto i = start; i < playlist.size(); i += 2)
				playlist[i]->play();
	}
};


#endif //JNP1_6_ODDEVENMODE_H
